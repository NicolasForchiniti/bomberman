﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
public class Game_Manager : MonoBehaviour
{

    private int bombs = 1;
    
    private static Game_Manager instance;  

    public static Game_Manager Get()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    // Update is called once per frame
    private void Update()
    {


    }
    public int PlayerBombs(int plusBomb) {
        
        bombs += plusBomb;
        return bombs;
    }
    public void GameOver() {
        if (Player_Controller.Get().lifes <= 0) {

        }
    }
    
}