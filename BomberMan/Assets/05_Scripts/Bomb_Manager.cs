﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb_Manager : MonoBehaviour
{

    public float rayDistance = 0.5f;
    public LayerMask rayCastLayer;
    public float countDown = 3f;
    public GameObject explodePrefab;
    public int damage = 1;
    private bool destroyBomb = false;
    void Update()
    {
        countDown -= Time.deltaTime;
        point_right();
        point_left();
        point_foward();
        point_background();

        if (countDown <= -0f)
        {
            destroyBomb = true;
        }
        if (destroyBomb) {
            Destroy(gameObject);
            Instantiate(explodePrefab, transform.position, Quaternion.identity);
            Player_Controller.Get().bombs++;
           
        }
    }

    public void point_left()
    {

        RaycastHit hit;

        if (Physics.Raycast(transform.position, -transform.right, out hit, rayDistance, rayCastLayer))
        {
            Debug.DrawRay(transform.position, -transform.right * hit.distance, Color.yellow);

            string layerHitted = LayerMask.LayerToName(hit.transform.gameObject.layer);

            if (layerHitted == "Box" && countDown <= 0f)
            {
                Destroy(hit.transform.gameObject);
                destroyBomb = true;
            }
            if (layerHitted == "Player" && countDown <= 0f)
            {
                Player_Controller.Get().lifes -= damage;
                //Destroy(hit.transform.gameObject);
                destroyBomb = true;
            }
            if (layerHitted == "Bomb" && countDown <= 0f)
            {
                Destroy(hit.transform.gameObject);     
                destroyBomb = true;
            }


        }
        else
        {
            Debug.DrawRay(transform.position, -transform.right * rayDistance, Color.red);
        }
    }

    public void point_right()
    {

        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.right, out hit, rayDistance, rayCastLayer))
        {
            Debug.DrawRay(transform.position, transform.right * hit.distance, Color.yellow);

            string layerHitted = LayerMask.LayerToName(hit.transform.gameObject.layer);

            if (layerHitted == "Box" && countDown <= 0f)
            {
                Destroy(hit.transform.gameObject);
                destroyBomb = true;
            }
            if (layerHitted == "Player" && countDown <= 0f)
            {
                Player_Controller.Get().lifes -= damage;
                // Destroy(hit.transform.gameObject);
                destroyBomb = true;
            }
            if (layerHitted == "Bomb" && countDown <= 0f)
            {
                Destroy(hit.transform.gameObject);
                destroyBomb = true;
            }

        }
        else
        {
            Debug.DrawRay(transform.position, transform.right * rayDistance, Color.red);
        }
    }

    public void point_background()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, -transform.forward, out hit, rayDistance, rayCastLayer))
        {
            Debug.DrawRay(transform.position, -transform.forward * hit.distance, Color.yellow);

            string layerHitted = LayerMask.LayerToName(hit.transform.gameObject.layer);

            if (layerHitted == "Box" && countDown <= 0f)
            {
                Destroy(hit.transform.gameObject);
                destroyBomb = true;
            }
            if (layerHitted == "Player" && countDown <= 0f)
            {
                Player_Controller.Get().lifes -= damage;
                // Destroy(hit.transform.gameObject);
                destroyBomb = true;
            }
            if (layerHitted == "Bomb" && countDown <= 0f)
            {
                Destroy(hit.transform.gameObject);
                destroyBomb = true;
            }
        }
        else
        {
            Debug.DrawRay(transform.position, -transform.forward * rayDistance, Color.red);
        }
    }

    public void point_foward()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, rayDistance, rayCastLayer))
        {
            Debug.DrawRay(transform.position, transform.forward * hit.distance, Color.yellow);

            string layerHitted = LayerMask.LayerToName(hit.transform.gameObject.layer);

            if (layerHitted == "Box" && countDown <= 0f)
            {
                Destroy(hit.transform.gameObject);
                destroyBomb = true;
            }
            if (layerHitted == "Player" && countDown <= 0f)
            {
                Player_Controller.Get().lifes -= damage;
                // Destroy(hit.transform.gameObject);
                destroyBomb = true;           
            }
            if (layerHitted == "Bomb" && countDown <= 0f)
            {
                Destroy(hit.transform.gameObject);
                destroyBomb = true;
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.forward * rayDistance, Color.red);
        }
    }
}
