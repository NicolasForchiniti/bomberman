﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataform_Manager : MonoBehaviour {

    public GameObject[] box;

    int randBox;

    // Use this for initialization
    void Start()
    {
        randBox = Random.Range(0, 1);
        Vector3 spawnPosition = new Vector3(Random.Range(-transform.position.x, transform.position.x), 2, Random.Range(-transform.position.z, transform.position.z));
        Instantiate(box[randBox], transform.position, gameObject.transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
