﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow_Target : MonoBehaviour {

    //Objeto a seguir
    public Transform target;

    //velocidad de camara
    Vector3 velocity = Vector3.zero;

    // tiempo a seguir el target
    public float smooothTime = 0.15f;

    //habilitar y configurar los valores maximos en Y
    public bool YMaxEnable = false;
    public float YMaxValue = 0;
    
    //habilitar y configurar los valores minimos en Y
    public bool YMinEnable = false;
    public float YMinValue = 0;

    //habilitar y configurar los valores maximos en Z
    public bool ZMaxEnable = false;
    public float ZMaxValue = 0;

    //habilitar y configurar los valores minimos en Z
    public bool ZMinEnable = false;
    public float ZMinValue = 0;

    //habilitar y configurar los valores maximos en X
    public bool XMaxEnable = false;
    public float XMaxValue = 0;
    
    //habilitar y configurar los valores minimos en X
    public bool XMinEnable = false;
    public float XMinValue = 0;


    private void FixedUpdate()
    {
        //posisicion del target
        Vector3 targetPos = target.position;

        //Up and Down
        if (YMinEnable && YMaxEnable)
            targetPos.y = Mathf.Clamp(target.position.y, YMinValue, YMaxValue);
 
        else if (YMinEnable)
            targetPos.y = Mathf.Clamp(target.position.y, YMinValue, target.position.y);

        else if(YMaxEnable)
            targetPos.y = Mathf.Clamp(target.position.y, target.position.y, YMinValue);
        
        //Vertical
        if (ZMinEnable && ZMaxEnable)
            targetPos.z = Mathf.Clamp(target.position.z, ZMinValue, ZMaxValue);

        else if (ZMinEnable)
            targetPos.z = Mathf.Clamp(target.position.z, ZMinValue, target.position.z);

        else if (ZMaxEnable)
            targetPos.z = Mathf.Clamp(target.position.z, target.position.z, ZMinValue);
       
        //Horizontal

        if (XMinEnable && XMaxEnable)
            targetPos.x = Mathf.Clamp(target.position.x, XMinValue, XMaxValue);

        else if (XMinEnable)
            targetPos.x = Mathf.Clamp(target.position.x, XMinValue, target.position.x);

        else if (XMaxEnable)
            targetPos.x = Mathf.Clamp(target.position.x, target.position.x, XMinValue);



        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, smooothTime);
    }
}
