﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Canvas_Manager : MonoBehaviour
{
    private float startTime;
    private string seconds;
    private string minutes;
    private float t;

    public Text Timer;
    public Sprite[] bombSprite;
    public Image bombUI;
    // Use this for initialization
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        t = Time.time - startTime;

        minutes = ((int)t / 60).ToString();
        seconds = (t % 60).ToString("f2");

        Timer.text = minutes + ":" + seconds;

        //bombUI.sprite = bombSprite[Player_Controller.Get().bombs];
    }
}
