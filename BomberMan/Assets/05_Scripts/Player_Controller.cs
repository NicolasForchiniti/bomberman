﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour {

    public float speed = 10f;
    public GameObject bombPrefab;
    private Rigidbody rg;
    public int lifes = 3;
    public int bombs = 1;
    private static Player_Controller instance;

    public static Player_Controller Get()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        rg = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {

        float hAxis = Input.GetAxis("Horizontal");
        float vAxis = Input.GetAxis("Vertical");

        Vector3 posBomb = new Vector3(0,0,0);
        posBomb.x = Mathf.Round(transform.position.x);
        posBomb.y = 0.6f;
        posBomb.z = Mathf.Round(transform.position.z);

        Vector3 movement = new Vector3(-vAxis, 0, hAxis) * speed * Time.deltaTime;

        rg.MovePosition(transform.position + movement);

        if (Input.GetKeyDown(KeyCode.Space)) {

            if (bombs == 1)
            {
                Instantiate(bombPrefab, posBomb, Quaternion.identity);
                bombs--;
            }
        }
	}
}
