﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_01 : MonoBehaviour {

    public enum EnemyState
    {   Idle,
        Patrol,
        Rotate,
        GoingToTarget,
        Last,
    }

    [SerializeField] private EnemyState state;

    public float speed = 2;
    public Transform target;
    public float rayDistance = 3f;
    public LayerMask rayCastLayer;

    private float rot = 90;
    private bool alarm = false;
    public float distanceToStop = 0;
    private float t;

    private void Update()
    {
        point_ray_foward();
        t += Time.deltaTime;
        switch (state)
        {
            case EnemyState.Idle:
                if (t > 1)
                {
                    NextState();
                }
                break;

            case EnemyState.Patrol:
                
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
                if (t > 2)
                    NextState();
                break;

            case EnemyState.Rotate:
                transform.Rotate(0, rot, 0);
                rot = 90;
                NextState();
                break;
            case EnemyState.GoingToTarget:

                if (alarm){
                    Vector3 dir = target.position - transform.position;
                    transform.Translate(dir.normalized * speed * Time.deltaTime);  
                }
                else {
                    NextState();
                }
                    
                break;
        }
    }

    private void NextState()
    {
        t = 0;
        int intState = (int)state;
        intState++;
        intState = intState % ((int)EnemyState.Last);
        SetState((EnemyState)intState);
    }

    private void SetState(EnemyState es)
    {
        state = es;
    }
    public void point_ray_foward()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, rayDistance, rayCastLayer))
        {
            Debug.DrawRay(transform.position, transform.forward * hit.distance, Color.yellow);

            string layerHitted = LayerMask.LayerToName(hit.transform.gameObject.layer);

            if (layerHitted == "Player")
            {
                SetState(EnemyState.GoingToTarget);
                alarm = true;
            }
            else {
                alarm = false;
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.forward * rayDistance, Color.red);
        }
    }
    public void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Block" || collision.gameObject.tag == "Box") {
            SetState(EnemyState.Rotate);
        }
    }
}
