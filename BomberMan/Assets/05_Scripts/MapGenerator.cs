﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {
    //****************************Variables publicas****************************
    public Transform tilePrefab;
    public Transform obstaclePrefab;
    public Transform blockPrefab;
    public Transform doorPrefab;
    public Transform powerUpPrefab;
    public Vector2 mapSize;
   

    [Range(0,1)] // Asiggno el rango que va a tener el Float
    public float outLinePercent;

    List<Coord> allTileCoords;
    Queue<Coord> shuffledTileCoords;

    public int seed ;
    public int Obstacles;

    private int door = 1;
    private int maxDoor = 1;
    private int maxPowerUps = 0;
    private int powerUps = 2;
    void Start () {
        MapGenerate();
        seed = Random.Range(0, 100000);
    }

    ////****************************Genera el mapa****************************
    public void MapGenerate() {

        allTileCoords = new List<Coord>();
        for (int x = 0; x < mapSize.x; x++){
            for (int y = 0; y < mapSize.y; y++){
                allTileCoords.Add(new Coord(x, y));
            }
        }
        shuffledTileCoords = new Queue<Coord> (Utility.ShuffleArray (allTileCoords.ToArray(), seed));

        string holderName = "Generate Map";
        if (transform.Find(holderName)) {
            DestroyImmediate(transform.Find(holderName).gameObject);
        }

        Transform mapHolder = new GameObject(holderName).transform;
        mapHolder.parent = transform;

        string holderBoxName = "Generate Boxes";
        if (transform.Find(holderBoxName))
        {
            DestroyImmediate(transform.Find(holderBoxName).gameObject);
        }

        Transform boxHolder = new GameObject(holderBoxName).transform;
        boxHolder.parent = transform;

        string holderBlocksName = "Generate Blocks";
        if (transform.Find(holderBlocksName))
        {
            DestroyImmediate(transform.Find(holderBlocksName).gameObject);
        }

        Transform blocksHolder = new GameObject(holderBlocksName).transform;
        blocksHolder.parent = transform;

        for (int x = 0; x < mapSize.x; x++){
            for (int y = 0; y < mapSize.y; y++){
                Vector3 tilePosition = CoordToPosition(x,y);
                Transform newTile = Instantiate(tilePrefab, tilePosition, Quaternion.Euler(Vector3.right * 90)) as Transform;
                newTile.localScale = Vector3.one * (1 - outLinePercent);
                newTile.parent = mapHolder;

                if ((y % 2) != 0 && (x % 2) != 0 && x > 0 && y > 0 && x < 20 && y < 30)
                {
                    Vector3 blockPosition = CoordToPosition(x, y);
                    Transform newBlock = Instantiate(blockPrefab, blockPosition + Vector3.up * 0.5f, Quaternion.identity) as Transform;
                    newBlock.parent = blocksHolder;
                }

            }
        }

        int obstacleCount = Obstacles;
        for (int i = 0; i < obstacleCount; i++){
            Coord randomCoord = GetRandomCoord();        
            Vector3 obstaclePosition = CoordToPosition(randomCoord.x, randomCoord.y);
            Transform newObstacle = Instantiate(obstaclePrefab, obstaclePosition + Vector3.up * 0.5f, Quaternion.identity) as Transform;
            if (door == maxDoor) {
                Transform newDoor = Instantiate(doorPrefab, obstaclePosition + Vector3.up * 0.5f, Quaternion.identity) as Transform;
                newDoor.parent = boxHolder;
                door--;
            }
            if (powerUps == maxPowerUps)
            {
                Transform newPowerUp = Instantiate(powerUpPrefab, obstaclePosition + Vector3.up * 0.5f, Quaternion.identity) as Transform;
                newPowerUp.parent = boxHolder;
            }
            powerUps--;
            newObstacle.parent = boxHolder;
            
        }
    }


    Vector3 CoordToPosition(int x, int y) {
        return new Vector3(-mapSize.x / 2 + 0.5f + x, 0, -mapSize.y / 2 + 0.5f + y);
    }
    public Coord GetRandomCoord() {
        Coord randomCoord = shuffledTileCoords.Dequeue();
        shuffledTileCoords.Enqueue(randomCoord);
        return randomCoord;
    }

    public struct Coord {
        public int x;
        public int y;

        public Coord(int _x, int _y) {
            x = _x;
            y = _y;
        }
    }
}
